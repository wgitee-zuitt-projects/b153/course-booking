const mongoose = require('mongoose');

//create the schema that each course must follow
const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required."]
	},
	description: {
		type: String,
		required: [true, "Course description is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date() //by using default: the createdOn field for each cource will automatically include the server timestamp of when the course was created.
	},
	enrollees: [{ //this says that the enrollees field is an array of objects and each object must contain a userID field. The timestamp of when each enrollee enrolled is automatically given. This also means that you can have multiple enrollees. 
		userId: {
			type: String,
			required: [true, "User ID is required."]
		},
		enrolledOn: {
			type: Date,
			default: new Date()
		}
	}]
})

//makes our model an exportable module that can be imported into different files
module.exports = mongoose.model("Course", courseSchema)