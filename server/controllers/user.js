const User = require("../models/user");
const Course = require("../models/course");
const bcrypt = require("bcrypt")
const auth = require("../auth") //auth.js file
//bcrypt is an NPM package that easily allows us to encrypt (and later on match) user passwords, since it is a very bad practice to store user passwords in plain text

module.exports.checkEmail = (body) => {
	//The mongoDB find() method ALWAYS returns an array
	return User.find({email: body.email}).then(result => {
		if(result.length > 0){ //if a duplicate email is found, result.length is 1. Otherwise, it is 0
			return true; //true means "yes, email exists"
		}else{
			return false; //false means "no, email does not exist"
		}
	})
}

/*ACTIVITY:

	Create a function named register that processes the user's submitted data and creates a new user record in our database. The process flow for this is very similar to creating a new course, with the main differences being the model to use and the data needed to create

	Check your work in Postman by creating a new route called "Register New User" and also check MongoDB Atlas if the user was created in the database.

	Once successfully tested, copy this file (controller/user) and paste it to activities/a1
*/

module.exports.register = (body) => {

	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		password: bcrypt.hashSync(body.password, 10),
		//bcrypt's hashSync method is the quickest solution to securely encrypting our users' passwords, so instead of directly passing the password, run it through hashSync first

		//The number 10 that we are also passing as an argument is called the "salt value."
		//The salt value is the number of types bcrypt will run its encryption algorithm on a user's password. 10 is enough times to have a speedy process and create a secure password. Any more than 10 simply gives diminishing returns.
		mobileNo: body.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})	
}

module.exports.login = (body) => {
	//findOne is a special Mongoose method that works like find() except only finds one record and returns an object, not an array
	return User.findOne({email: body.email}).then(result => {
		// console.log(result)
		if(result === null){ //user does not exist in db
			return false;
		}else{
			//bcrypt's compareSync method is the only way to check if the password being submitted by the user who is logging in is the same as their encrypted password in our db

			//If the passwords match, compareSync will return true. If not, it returns false
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){ //if passwords match
				return {accessToken: auth.createAccessToken(result.toObject())}
				//pass the user's data to createAccessToken to turn the data we need into a JSON Web Token
			}else{
				return false //passwords don't match
			}
		}
	})
}

module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined //reassign the value of the user's password to undefined, so that it does not show (for security purposes)
		return result
	})
}

module.exports.enroll = async (userId, body) => {
	//parts of an async (asynchronous) function can be given the await keyword.
	//any statement that is assigned the await keyword must first completely resolve before the function moves on to any statements without an await keyword.
	//in this function, saving our new user record AND our new course record must both be resolved before the function can return anything  

	let userSaveStatus = await User.findById(userId).then(user => {
		//retrieve the user's data record from the db and then using dot notation, access their enrollments array. Use .push() to add a new object containing the courseId to that array
		user.enrollments.push({courseId: body.courseId})

		return user.save().then((user, error) => {
			//save the changes to the user's data, then check for errors. If there is an error in saving, userSaveStatus' value will be false

			//If there is no error, userSaveStatus' value will be true
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})

	//the below process is almost exactly the same as above, only for the course
	let courseSaveStatus = await Course.findById(body.courseId).then(course => {
		course.enrollees.push({userId: userId})

		return course.save().then((course, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})

	//both userSaveStatus AND courseSaveStatus must be true (meaning both operations succeeded) for our enroll function to return true. Else, it will return false
	if(userSaveStatus && courseSaveStatus){
		return true;
	}else{
		return false;
	}
}
