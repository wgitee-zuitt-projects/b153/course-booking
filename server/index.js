//set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
//import routes
const courseRoutes = require("./routes/course")
const userRoutes = require("./routes/user")

//add the database connection
mongoose.connect("mongodb+srv://admin:admin@testdatabase1.zq0wb.mongodb.net/course_booking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//database connection confirmation message
mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."))

//server setup
const app = express()

//middlewear that allows our app to receive nested JSON data
app.use(express.json())
app.use(express.urlencoded({
	extended: true
}))

//activate cors
app.use(cors())

//add imported routes
app.use("/courses", courseRoutes)
app.use("/users", userRoutes)

const port = 4000

app.listen(process.env.PORT || port, () => {
	console.log(`Server running on port ${port}`)
})