const express = require("express");
const router = express.Router()
const userController = require("../controllers/user");
const auth = require("../auth");

//route for checking if an email exists in our database
router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
})

//route for user registration
router.post("/register", (req, res) => {
	// console.log(req.body)
	userController.register(req.body).then(resultFromController => res.send(resultFromController))
})

//route for login
router.post("/login", (req, res) => {
	console.log(req.body)
	userController.login(req.body).then(resultFromController => res.send(resultFromController))

})

//route for getting user profile
router.get("/details", auth.verify, (req, res) => {
	//get user's ID from the token
	const userId = auth.decode(req.headers.authorization).id

	userController.getProfile(userId).then(resultFromController => res.send(resultFromController))
})

//route for enrollment
router.post("/enroll", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id
	//userId is a variable that represents the user's ID property that is stored in their JSON web token, and decoded with auth.decode
	//req.body contains the courseId
	userController.enroll(userId, req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router;
